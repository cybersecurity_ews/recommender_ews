""" Implements Content based filter using tags
autor: Rodrigo Campiolo
"""

class ContentFiltering:
    def __init__(self):
        self.user_tag = {}    #dict userid --> dict tags-->weight
        
    def build_user_tag_weight(self, ratings):
        """ ratings - list of Rating
            Preenche a estrutura user_tag a partir de uma lista de Rating
        """
        for rating in ratings:
            if rating.userid not in self.user_tag.keys():
                self.user_tag[rating.userid] = {rating.provider:1, rating.product:1}
            else:
                tag_weight = self.user_tag[rating.userid]
                if rating.provider not in tag_weight.keys():
                    tag_weight[rating.provider] = 1
                else:
                    tag_weight[rating.provider] += 1    
                    
                if rating.product not in tag_weight.keys():
                    tag_weight[rating.product] = 1
                else:
                    tag_weight[rating.product] += 1 
                    
    def get_total_user_tag_weight(self, userid, topn_tags=0):
        """ userid - id do usuario
            topn_tags - somar as topn tags do usuario, se 0 soma todas as tags
            return soma das topn tags do usuario
        """
        tag_weight = self.user_tag[userid]
        
        if topn_tags != 0:
            return sum(sorted(tag_weight.values(),reverse=True)[:topn_tags])
               
        return sum(tag_weight.values())
    
    def get_item_user_tag_weight(self, userid, item_tags):
        """ userid - id do usuario
            item_tags - lista ou conjunto de tags de um item
            return soma do peso das tags do usuario que estao no item
        """
        tag_weight = self.user_tag[userid]
        item_weight = 0
        for item_tag in item_tags:
            if item_tag in tag_weight.keys():
                item_weight += tag_weight[item_tag]
        
        return item_weight
    
    def get_topn_user_tags(self, userid, topn_tags):
        """ userid - id do usuario
            topn_tags - topn tags do usuario
            return retorna em ordem decrescente as topn tags de maior peso
        """
        tag_weight = self.user_tag[userid]
        return sorted(tag_weight,key=tag_weight.__getitem__, reverse=True)[:topn_tags]

    def calc_topn_score_recomendation(self, userid, alerts, n):
        """ userid - id do usuario
            alerts - list of alerts (ratings)
            n - topn recomendacoes
        """
        scores = {} #dict alertid --> score 
        
        score_user_tag_weight = self.get_total_user_tag_weight(userid)
        for rating in alerts:
            scores[rating.alertid] = self.get_item_user_tag_weight(userid, rating.tags())/float(score_user_tag_weight)
            
        return sorted(scores,key=scores.__getitem__, reverse=True)[:n]
        
        
            
            
    