"""Implements a recommender system
Rodrigo Campiolo
10-08-2016
"""

import csv

from collaborativefiltering import CollaborativeFiltering
from contentfiltering import ContentFiltering
from generalranking import GeneralRanking
from rating import Rating
        

def main():
    list_ratings = []
    
    # read dataset and store in a list
    with open('../dataset/ratings_84Kratings_1000Users_2000Alerts.csv', 'r') as csvfile:
        reader = csv.reader(csvfile)
        print reader.next()
    
        for row in reader:
            try:
                rating = Rating(row)
                list_ratings.append(rating)
            except:
                print row
    
    print "Number of ratings: ", len(list_ratings)
    for rating in list_ratings[:5]:
        print rating;

    contentfilter = ContentFiltering()
    contentfilter.build_user_tag_weight(list_ratings)
    print(contentfilter.user_tag[1])
    print("Top tags: ", contentfilter.get_topn_user_tags(1, topn_tags=5))
    print("Weight total tags: ", contentfilter.get_total_user_tag_weight(1,topn_tags=5))  
        
    cfilter = CollaborativeFiltering()
    cfilter.build_user_alert_matrix(list_ratings)
    print(cfilter.user_like_alert[1])
    print(cfilter.user_dislike_alert[1])
     
    cfilter.build_user_similarity_matrix()
    print(cfilter.user_similarity[1,2])
    print(cfilter.user_similarity[1,1])
     
    vizinhos = cfilter.get_n_neighbors_users(1,n=20)
    print("vizinhos:", vizinhos)
 
    potencial_alerts = cfilter.select_alerts_to_user(1, vizinhos)
    print(potencial_alerts)
     
    topn = cfilter.calc_topn_score_recomendation(1, vizinhos, potencial_alerts, n=5)
    print(topn)
    
    general_ranking = GeneralRanking()
    general_ranking.calc_ranking(list_ratings)
    
    for rating in general_ranking.ratings_all.values()[:20]:
        print rating, rating.get_rating()

    #gera uma nova lista ordenada pelo rating
    new_list = sorted (general_ranking.ratings_all.values(), key=lambda x: x.get_rating(), reverse=True) 

    for r in new_list[:20]:
        print r, r.get_rating()
    l = general_ranking.get_list_score()
    for r in l[:20]:
        print r, r.ranking_score
      
    
    
if __name__ == '__main__':
    main()    

