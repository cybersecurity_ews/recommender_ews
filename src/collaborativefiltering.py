""" Implements Collaborative Filter
autor: Rodrigo Campiolo
"""

class CollaborativeFiltering:
    
    def __init__(self):
        self.user_like_alert = {}     # dict userid --> set alert
        self.user_dislike_alert = {}  # dict userid --> set alert
        self.user_critical_alert = {} # dict userid --> set alert
        self.user_similarity = {}     # matriz similaridade usuarios, dict (useridA,useridB)-->sim 
    
        
    def build_user_alert_matrix(self, ratings):
        """ ratings - list of Rating
            Preenche as estruturas user_like_alert, user_dislike_alert e user_critical_alert
        """
        for rating in ratings:
            if rating.like:
                if rating.userid not in self.user_like_alert:
                    self.user_like_alert[rating.userid] = set([rating.alertid])
                else:
                    self.user_like_alert[rating.userid].add(rating.alertid)
            elif rating.dislike:
                if rating.userid not in self.user_dislike_alert:
                    self.user_dislike_alert[rating.userid] = set([rating.alertid])
                else:
                    self.user_dislike_alert[rating.userid].add(rating.alertid)
            if rating.critical:
                if rating.userid not in self.user_critical_alert:
                    self.user_critical_alert[rating.userid] = set([rating.alertid])
                else:
                    self.user_critical_alert[rating.userid].add(rating.alertid)
                
     
    def build_user_similarity_matrix(self, users=[]):
        """ users - usuarios presentes na matriz, se vazio, sao todos os usuarios
            Preenche a matriz de similaridade usuario x usuario
        """
        if len(users) == 0:
            users = self.user_like_alert.keys()
        
        for userid_a in users:
            alerts_a = self.user_like_alert[userid_a]
            for userid_b in users:
                alerts_b = self.user_like_alert[userid_b]
                score = self.jaccard(alerts_a,alerts_b)
                self.user_similarity[userid_a,userid_b] = score
                
                
    def jaccard(self, A, B):
        """ A - set of Rating.alertid do usuario A
            B - set of Rating.alertid do usuario B
            return a similaridade entre dois usuarios usando Jaccard
        """
        return len(A.intersection(B))/float(len(A.union(B)))
    
    def get_n_neighbors_users(self, userid, n=0, users=[]):
        """ userid - id do usuario
            n - numero de vizinhos, se 0, considera todos usuarios
            users - considera essa lista como o conjunto de usuarios que sao vizinhos
            retorn lista de vizinhos (tuplas) do mais similar para o menos similar
        """
        if len(users) == 0:
            users = self.user_like_alert.keys()
        
        if n == 0:
            n = len(users)
        
        neighbors_list = {}
        for user in users:
            if user != userid:
                neighbors_list[userid,user] = self.user_similarity[userid,user]
        
        
        return [b for a,b in sorted(neighbors_list, key=neighbors_list.__getitem__, reverse=True)[:n]]
         
    def select_alerts_to_user (self, userid, neighbors):
        """ userid - id do usuario
            neighbors - lista de vizinhos (userid dos vizinhos)
            return alertas nao visualizados pelo usuario (set of Rating.alertid)
        """
        # junta os alertas dos vizinhos
        potencial_alerts = set()
        for neighbor in neighbors:
            potencial_alerts.update(self.user_like_alert[neighbor])

        # retorna os alertas nao visualizados pelo usuario (like e dislike)
        return potencial_alerts.difference(self.user_like_alert[userid].union(self.user_dislike_alert[userid]))
    
    def calc_topn_score_recomendation(self,userid,neighbors,potencial_alerts,n=0):
        """ userid - id do usuario
            neighbors - lista de vizinhos (userid dos vizinhos)
            potencial_alerts - conjunto de alertas potenciais a serem recomendados
            n - numero de alertas a ser recomendado, se 0, retorna todos
            return os alertas com maior score para um usuario
        """
        for user in neighbors:
            jaccard_sum = self.user_similarity[userid,user]
        
        alert_score_map = {}
        for alert in potencial_alerts:
            alert_score = 0;
            for user in neighbors:
                user_score = int(alert in self.user_like_alert[user]) + 4*int(alert in self.user_critical_alert[user]) - 2*int(alert in self.user_dislike_alert[user]) 
                alert_score += (user_score * self.user_similarity[userid,user]);
            alert_score_map[alert] = alert_score/float(jaccard_sum)
        
        if n == 0:
            n = len(alert_score_map)
        
        print(alert_score_map)
        return sorted(alert_score_map, key=alert_score_map.__getitem__, reverse=True)[:n]

        #return alert_score_map
