"""
Implements  two classes used in the Recommender: Rating and AlertRatingStat 
"""

class Rating:
    def __init__(self, rating):
        self.userid = int(rating[0])
        self.alertid = rating[1]
        self.provider = rating[2]
        self.product = rating[3]
        self.cvss_score = rating[4]
        self.like = int(rating[5])
        self.dislike = int(rating[6])
        self.critical = int(rating[7])

    def __str__(self):
        return 'USER={},CVE={},TAGS=({},{}),SCORE={},L={},D={},C={}'.format(self.userid,self.alertid,self.provider,self.product,self.cvss_score,self.like,self.dislike,self.critical)


    def tags(self):
        return [self.provider, self.product]


class AlertRatingStat:
    def __init__(self, alertid):
        self.alertid = alertid
        self.num_like = 0
        self.num_dislike = 0
        self.num_critical = 0
        
    def __str__(self):
        return "{} --> L={} D={} C={}".format(self.alertid,self.num_like,self.num_dislike,self.num_critical)
    
    def compute_rating(self,rating):
        self.num_like += rating.like
        self.num_dislike += rating.dislike
        self.num_critical += rating.critical

    def get_rating(self):
        return 4*self.num_critical - 2*self.num_dislike + self.num_like
    
    def get_num_votes(self):
        return self.num_like + self.num_dislike + self.num_critical
    
    def get_num_ratings(self):
        return self.num_like + self.num_dislike
