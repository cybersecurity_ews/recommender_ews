"""
Implements General Ranking
autor: Rodrigo Campiolo
"""

from rating import AlertRatingStat


class GeneralRanking:
    def __init__(self):
        self.ratings_all = {}    #dict alertid to AlertRatingStat
        self.total_ratings = 0   # total de ratings para o alerta
        self.total_votes = 0     # total de votos para o alerta

    def calc_ranking(self, ratings):
        """ ratings - list of Rating
            Preenche as estruturas da classe.
        """
        for rating in ratings:
            if rating.alertid not in self.ratings_all.keys():
                alert_rating_stat = AlertRatingStat(rating.alertid)
            else:
                alert_rating_stat = self.ratings_all[rating.alertid]
            alert_rating_stat.compute_rating(rating)
            self.ratings_all[rating.alertid] = alert_rating_stat
            self.total_ratings += alert_rating_stat.get_num_ratings()
            self.total_votes += alert_rating_stat.get_num_votes()
    
    def get_list_score(self):
        """ return uma lista classificada com os scores
        """
        ranking = []
        for rating in self.ratings_all.values():
            votes_avg = rating.get_num_votes()/self.total_votes
            rating_avg = rating.get_num_ratings()/self.total_ratings
            
            score = ((votes_avg*rating_avg) + (rating.get_num_votes() * rating.get_rating()))/float(self.total_ratings+votes_avg)
            rating.ranking_score = score
            ranking.append(rating)
            
        # classifica a lista
        ranking.sort(key=lambda x: x.get_rating(), reverse=True)
                   
        return ranking
