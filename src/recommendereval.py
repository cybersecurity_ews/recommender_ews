"""Evaluates a recommender system
Rodrigo Campiolo
19-08-2016
"""

import csv

from collaborativefiltering import CollaborativeFiltering
from contentfiltering import ContentFiltering
from generalranking import GeneralRanking
from rating import Rating
        

def main():
    list_ratings = []
    dict_ratings = {}  # userid --> list of Rating
    
    # read dataset and store in a list
#    with open('../dataset/ratings_84Kratings_1000Users_2000Alerts.csv', 'r') as csvfile:
    with open('../dataset/ratings_8KRatings_100Users_500Alerts.csv', 'r') as csvfile:
        reader = csv.reader(csvfile)
        #print reader.next()
    
        for row in reader:
            try:
                rating = Rating(row)
                
                if rating.userid not in dict_ratings:
                    dict_ratings[rating.userid] = [rating]
                else:
                    dict_ratings[rating.userid].append(rating)
                
                list_ratings.append(rating)
            except:
                print row
    
    print "Number of users: ", len(dict_ratings)
    
#    PERC_TRAIN = 0.75  # 75% eh usado para treino
    PERC_TRAIN = 0.50  # 50% eh usado para treino
    TOPN = 20
    N_NEIGHBORS = 20
    
    # build train and evaluation sets (split 75% e 25%)
    train_ratings = []
    evaluation_ratings = {}
    for userid in dict_ratings:
        temp_ratings = dict_ratings[userid]
        train_size = int(len(temp_ratings)*PERC_TRAIN) 
        train_ratings.extend(temp_ratings[:train_size]) #pega os primeiros elementos para o treino
        evaluation_ratings[userid] = temp_ratings[train_size:]
    
    ## Content filter
    contentfilter = ContentFiltering()
    contentfilter.build_user_tag_weight(train_ratings)
    
    ## Collaborative filter
    cfilter = CollaborativeFiltering()
    cfilter.build_user_alert_matrix(train_ratings)
    cfilter.build_user_similarity_matrix()
    
    
    print("---- EVALUATION ----")

    f = open('../dataset/results_recomendation.csv', 'w')
    f.write('"userid","evaluation_like_set","collaborative_likes","content_likes","evaluation_dislike_set","collaborative_dislikes","content_dislikes"\n')

    true_positive = false_positive = true_negative = false_negative = 0
    total_likes = total_dislikes = 0
    
    for userid in contentfilter.user_tag:
        vizinhos = cfilter.get_n_neighbors_users(userid,n=N_NEIGHBORS)
        potencial_alerts = cfilter.select_alerts_to_user(userid, vizinhos)
     
        topn_collaborative = set(cfilter.calc_topn_score_recomendation(userid, vizinhos, potencial_alerts, n=TOPN))
        topn_content = set(contentfilter.calc_topn_score_recomendation(userid, list_ratings, n=TOPN))

        evaluation_set = select_likes_user(evaluation_ratings[userid])
        print("Userid: ", userid)
        print("Evaluation: ", len(evaluation_set), evaluation_set)
        print("Collaborative: ", topn_collaborative)
        print("Content: ", topn_content)
        print("User preferences: ", contentfilter.get_topn_user_tags(userid, topn_tags=10))
        
        print("Collaborative - Itens em comum: ", evaluation_set.intersection(topn_collaborative))
        print("Content - Itens em comum: ", evaluation_set.intersection(topn_content))
        
        dislike_evaluation_set = select_dislikes_user(evaluation_ratings[userid])
        print("Collaborative dislike: ", dislike_evaluation_set.intersection(topn_collaborative))
        print("Content dislike: ", dislike_evaluation_set.intersection(topn_content))

        print("------")
        
        #store stats
        like_collaborative_result = evaluation_set.intersection(topn_collaborative)
        like_content_result = evaluation_set.intersection(topn_content)
        dislike_collaborative_result = dislike_evaluation_set.intersection(topn_collaborative)
        dislike_content_result = dislike_evaluation_set.intersection(topn_content)
        
        true_positive += len(like_collaborative_result.union(like_content_result))
        false_positive += len(dislike_collaborative_result.union(dislike_content_result))
        total_dislikes += len(dislike_evaluation_set)
        total_likes += len(evaluation_set)
        
        #save results in a file
        f.write("{},{},{},{},{},{},{}\n".format(userid, len(evaluation_set), 
                 len(evaluation_set.intersection(topn_collaborative)),
                 len(evaluation_set.intersection(topn_content)),
                 len(dislike_evaluation_set),
                 len(dislike_evaluation_set.intersection(topn_collaborative)),
                 len(dislike_evaluation_set.intersection(topn_content))))
        
    f.close()
    
    true_negative += total_dislikes - false_positive #rever o sentido de fazer isso
    false_negative += total_likes - true_positive

    print("\n---- Statistics ----")
    print("Total likes: ", total_likes)
    print("Total dislikes: ", total_dislikes)
    print("--------------------")
    print("True positives: " , true_positive)
    print("False positives: " , false_positive)
    print("True negatives: " , true_negative) #não tem validade na recomendação
    print("False negatives: ", false_negative)
    print("--------------------")
    print("Precision: ", true_positive/float(true_positive + false_positive))
    print("Recall: ", true_positive/float(true_positive + false_negative))
    
    
def select_likes_user(ratings):
    return set(rating.alertid for rating in ratings if rating.like)    

def select_dislikes_user(ratings):
    return set(rating.alertid for rating in ratings if rating.dislike)    
    
if __name__ == '__main__':
    main()    

